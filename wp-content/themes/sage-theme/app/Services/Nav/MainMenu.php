<?php

namespace App\Services\Nav;

class MainMenu {
  public $menuName;

  public function __construct($menuName) {
    $this->menuName = $menuName;
  }

  public function getMenu() {
    return wp_get_nav_menu_items($this->menuName);
  }
}