<?php

namespace App\Services\Nav;


class C8ThemeHeaderMenu extends \Walker_Nav_Menu
{
  public function end_el( &$output, $item, $depth = 0, $args = null ) {
    if ($item->menu_item_parent === '0') {
      $file = locate_template(["views/partials/filters/dropdown-header-menu.blade.php", "dropdown-header-menu.blade.php"]);
      $output .= \App\sage('blade')->render($file, []);
    }
    $output .= "</li>";
  }
}
