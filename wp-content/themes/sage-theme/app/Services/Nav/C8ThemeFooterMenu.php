<?php

namespace App\Services\Nav;

/*
 *
 * Custom navigation for this theme.
 *
 */

class C8ThemeFooterMenu extends \Walker_Nav_Menu
{  public function end_el( &$output, $item, $depth = 0, $args = null ) {
    if ($item->title === 'Contact') {
      $file = locate_template(["views/partials/contact.blade.php", "contact.blade.php"]);
      $output .= \App\sage('blade')->render($file, []);
    }
    $output .= "</li>";
  }
}
