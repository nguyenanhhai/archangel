<?php

namespace App\Controllers\Modules;

class CtaBanner
{
    public function dataModule($module)
    {
        return (object) [
            'module' => $module,
            'image' => $module['image'],
            'content' => $module['content'],
            'link1' => $module['link1'],
            'link2' => $module['link2']
        ];
    }
}
