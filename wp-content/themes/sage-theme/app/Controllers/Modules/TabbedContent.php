<?php

namespace App\Controllers\Modules;

class TabbedContent
{
    public function dataModule($module)
    {
        return (object) [
            'module' => $module,
            'title' => $module['title'],
            'tabs' => $this->mapTab($module['tabs'])
        ];
    }

    protected function mapTab($tabsData) {
        return array_reduce(get_object_vars($tabsData), function($acc, $cur) {
            if ($cur !== null) {
                array_push($acc, $cur);
            }
            return $acc;
        }, array());
    }
}
