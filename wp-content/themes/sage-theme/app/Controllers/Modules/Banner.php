<?php

namespace App\Controllers\Modules;

class Banner
{
    public function dataModule($module)
    {
        return (object) [
            'module' => $module,
            'content' => $module['content'],
            'link' => $module['link'],
            'imageBig' => $module['image_big'],
            'imageSmall' => $module['image_small'],
        ];
    }
}
