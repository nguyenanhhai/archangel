<?php

namespace App\Controllers\Modules;

class GenesisgeneCallOut
{
    public function dataModule($module)
    {
        return (object) [
            'module' => $module,
            'title' => $module['title'],
            'content' => $module['content'],
            'icon' => $module['icon'],
            'hasIcon' => $module['has_icon'],
            'position' => $module['position'],
            'link' => $module['link'],
            'image' => $module['image']
        ];
    }
}
