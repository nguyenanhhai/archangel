<?php

namespace App\Controllers\Modules;

class Industries
{
    public function dataModule($module)
    {
        return (object) [
            'module' => $module,
            'title' => $module['title'],
            'industries' => $module['industries'],
        ];
    }
}
