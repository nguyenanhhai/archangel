<?php

/**
 * Define constant
 */

define('TEMPLATE_URL', get_template_directory_uri());
define('RESOURCES_PATH', get_stylesheet_directory());
define('NAME_THEME_URL', str_replace('/resources', '', get_template_directory_uri()));
if (defined('IS_WP_PROD') && IS_WP_PROD) {
    define('TEMPLATE_ASSETS_URL', get_template_directory_uri() . '/assets/dist');
} else {
    define('TEMPLATE_ASSETS_URL', get_template_directory_uri() . '/assets');
}
define('IMG_BASE64', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC');
define('IMG_CLOSE', 'data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIxNi45NzEiIGhlaWdodD0iMTYuOTcxIiB2aWV3Qm94PSIwIDAgMTYuOTcxIDE2Ljk3MSI+CiAgPGcgaWQ9InBsdXMiIHRyYW5zZm9ybT0idHJhbnNsYXRlKC01LjY1NyA4LjQ4NSkgcm90YXRlKC00NSkiPgogICAgPGcgaWQ9Ikdyb3VwXzE1IiBkYXRhLW5hbWU9Ikdyb3VwIDE1Ij4KICAgICAgPHBhdGggaWQ9IlBhdGhfMiIgZGF0YS1uYW1lPSJQYXRoIDIiIGQ9Ik0uNjY3LDBWMjAiIHRyYW5zZm9ybT0idHJhbnNsYXRlKDkuMzMzKSIgZmlsbD0ibm9uZSIgc3Ryb2tlPSIjYmMxMTFmIiBzdHJva2UtbGluZWNhcD0icm91bmQiIHN0cm9rZS1taXRlcmxpbWl0PSIxMCIgc3Ryb2tlLXdpZHRoPSIyIi8+CiAgICAgIDxwYXRoIGlkPSJQYXRoXzItMiIgZGF0YS1uYW1lPSJQYXRoIDIiIGQ9Ik0yMCwuNjY3SDAiIHRyYW5zZm9ybT0idHJhbnNsYXRlKDAgOS4zMzMpIiBmaWxsPSJub25lIiBzdHJva2U9IiNiYzExMWYiIHN0cm9rZS1saW5lY2FwPSJyb3VuZCIgc3Ryb2tlLW1pdGVybGltaXQ9IjEwIiBzdHJva2Utd2lkdGg9IjIiLz4KICAgIDwvZz4KICA8L2c+Cjwvc3ZnPgo=');
define('S3_FONT', 'https://fonts.googleapis.com/css?family=Lato:400,700,900&display=swap');
define('ACF_OPTION', 'option');
define('SCREEN', 'screen');

// define in RegisterPostType.php
define('ICON_ID', 'icon_id');
define('FEATURE_IMG_NAME', 'feature_img_name');
define('REWRITE_SLUG', 'rewrite_slug');
define('TAXONOMY_NAME', 'taxonomy_name');
define('TAXONOMY_SLUG', 'taxonomy_slug');

// define in Helpers.php
define('PREG_MATCH_YOUTUBE', "/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:m\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)/");
define('PREG_MATCH_VIMEO', '%^https?:\/\/(?:www\.|player\.)?vimeo.com\/(?:channels\/(?:\w+\/)?|groups\/([^\/]*)\/videos\/|album\/(\d+)\/video\/|video\/|)(\d+)(?:$|\/|\?)(?:[?]?.*)$%im');

// define name menu
define('MENU_HEADER', 'Menu Header');
define('MENU_FOOTER', 'Menu Footer');
