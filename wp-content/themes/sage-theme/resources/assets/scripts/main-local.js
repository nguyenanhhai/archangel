// import external dependencies
// import $ from 'jquery';
import 'slick-carousel/slick/slick'
import 'jquery-lazy/jquery.lazy'
import 'bootstrap/js/dist/collapse'

// import modules
import './modules/FixHeight';
import './modules/UserAgent';
import './modules/Header';
import './modules/Menu';
import './modules/Popup';
import './modules/SelectC8';
import './modules/LazyLoadImage';
import './modules/TabbedContent';

// if ($(window).width()<=1024 && (document.cookie="ready-ani=true")) {
//   $("#loadding-page").addClass("hide-loader")
// }

// import './loading';

console.log('Main Localhost')