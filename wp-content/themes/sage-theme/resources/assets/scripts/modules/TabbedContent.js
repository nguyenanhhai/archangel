const internals = {
  /**
   * Variable
   */
  $tabs: $('#tabbedContent'),
  /**
   * Function
   */
  addEventRotaCloseBtn() {
    this.$tabs.find('.tab-content').on('show.bs.collapse', function() {
      $(this).prev().find('.tab-header').addClass('tab-header-active')
    })

    this.$tabs.find('.tab-content').on('hide.bs.collapse', function() {
      $(this).prev().find('.tab-header').removeClass('tab-header-active')
    })
  }
}

const TabbedContent = (() => {
  if (internals.$tabs.length) {
    internals.addEventRotaCloseBtn()
  }
})()

export default TabbedContent
