<div class="social">
  @foreach ($data as $social)
    <a href="{{$social['link']}}">
    <i class="icomoon icon-{{$social['icon']}}"></i>
    </a>
  @endforeach
</div>