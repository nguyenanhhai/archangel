<div class="mt-4 d-md-none d-lg-block d-xl-flex">
  <div class="wraper-address">
    {!! App::getFooterAddress() !!}
  </div>
  
  <div class="mt-5 mt-xl-0 ml-xl-5">
    <h3>Flow us</h3>
    @include('partials.social', ['data' => App::getSocials()])
  </div>
</div>