{{-- <div class="space-100"></div> --}}
{{-- @php
    $menuFooter = (new App\Services\Nav\MainMenu(MENU_FOOTER))->getMenu();
@endphp --}}

<footer id="footer" class="module footer mt-auto bg-secondary container pt-5 pb-4">
  <div class="px-2">
    <img src="{{IMG_BASE64}}" data-src="{{App::getFooterLogo()['url']}}" alt="{{App::getFooterLogo()['alt']}}" class="lazy footer-logo">
  </div>
  <div class="row my-4 my-lg-5 ps-rv">
    <img src="{{IMG_BASE64}}" data-src="{{App::getFooterLogo2()['url']}}" alt="{{App::getFooterLogo2()['alt']}}" class="lazy footer-logo2 d-none d-xl-block">
    <div class="col-12 col-md-6 footer-menu col-lg-12">
      <div class="px-2 d-lg-flex justify-content-lg-between">
        @php
        wp_nav_menu(array(
          'theme_location' => 'footer',
          'menu' => MENU_FOOTER,
          'items_wrap' => '%3$s',
          'container' => false,
          'depth' => 2,
          'walker' => new App\Services\Nav\C8ThemeFooterMenu()
          ));
        @endphp
  
      </div>

    </div>
    <div class="d-none d-md-block d-lg-none col-md-6">
      <div class="wraper-address">
        {!! App::getFooterAddress() !!}
      </div>
      
      <div class="mt-5 mt-xl-0 ml-xl-5">
        <h3>Flow us</h3>
        @include('partials.social', ['data' => App::getSocials()])
      </div>
    </div>
  </div>
  
  <div class="text-center text-md-left mt-5">
    <div class="footer-copyright last-mb-none">
      {!! App::getCopyRight() !!}
    </div>
  </div>

</footer>

<!-- endblock -->
<noscript>
  <div id="mod-noscript" class="mod-noscript">
    <div class="d-table w-100 h-100">
      <div class="d-table-cell align-middle text-center">
        <div class="container">
          <h3>To use web better, please enable Javascript.</h3>
        </div>
      </div>
    </div>
  </div>
</noscript>


