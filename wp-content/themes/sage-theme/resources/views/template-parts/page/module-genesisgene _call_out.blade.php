<section class="module mod-genesisgene-call-out bg-pink">
  <div class="container ps-rv genesisgene">
    <div class="content @if ($data->position === 'right') content-right  @endif">
      <div>
        <img class="adn-icon lazy @if (!$data->hasIcon) d-none  @endif" src="{{IMG_BASE64}}" data-src="{{$data->icon}}" alt="adn">
      </div>
      
      <h2>{{$data->title}}</h2>
      <div class="my-3 d-lg-none">
        <img class="lazy" src="{{IMG_BASE64}}" data-src="{{$data->image}}" alt="{{$data->title}}">
      </div>
      {!! $data->content !!}
      <div class="text-center text-md-left">
        <a href="{{$data->link}}" class="btn btn-outline-primary">View details</a>
      </div>
    </div>
  <div class="lazy d-none d-lg-block bg bg-center bg-featured @if ($data->position === 'right') bg-featured-left  @endif" data-src="{{$data->image}}"></div>
  </div>
</section>
