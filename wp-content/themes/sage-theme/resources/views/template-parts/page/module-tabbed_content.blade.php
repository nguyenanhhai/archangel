<section class="module mod-tabbed">
  <div class="container-2 container-3">
    {!!$data->title!!}

    <div class="list-tabbed d-none d-lg-flex align-items-ends justify-content-between border-bottom">
      @foreach ($data->tabs as $tab)
        <a class="pb-2 ps-rv @if($loop->index !== 0) collapsed @endif" href="javascript:void(0);" data-toggle="collapse" data-target="#tab-{{$loop->index}}" aria-expanded="false" aria-controls="tab-{{$loop->index}}">
          <span>{{$tab->tab_name}}</span>
        </a>
      @endforeach
    </div>
    
    <div class="accordion pt-lg-4" id="tabbedContent">
      @foreach ($data->tabs as $tab)
        <div class="card overflow-unset">
          <div class="card-header" id="tab-heading-{{$loop->index}}">
            <h3 class="mb-0 pr-5 ps-rv tab-header d-lg-none @if($loop->index === 0) tab-header-active @else collapsed @endif" data-toggle="collapse" data-target="#tab-{{$loop->index}}" aria-expanded="false" aria-controls="tab-{{$loop->index}}">
                {{$tab->tab_name}}
            </h3>
          </div>
          <div id="tab-{{$loop->index}}" class="collapse @if($loop->index === 0) show @endif tab-content" aria-labelledby="tab-heading-{{$loop->index}}" data-parent="#tabbedContent">
            <div class="card-body py-3 d-lg-flex align-items-lg-center">
              <div class="text-center d-lg-none">
                <img src="{{IMG_BASE64}}" data-src="{{$tab->image}}" alt="{{$tab->tab_name}}" class="lazy w-100">
              </div>
              <div data-src="{{$tab->image}}" class="bg-tab bg bg-center col-7 lazy d-none d-lg-block"></div>
              <div class="wrap-content my-3 p-lg-3">
                {!!$tab->content!!}
                <div class="text-center text-lg-left col-lg-5">
                  <a href="{{$tab->link}}" class="btn btn-outline-primary">View details</a>
                </div>
              </div>
            </div>
          </div>
        </div> 
      @endforeach
    </div>
  </div>
</section>
