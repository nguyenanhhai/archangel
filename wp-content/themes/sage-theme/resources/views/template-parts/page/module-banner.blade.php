<section class="module mod-banner">
  <div class="container-2">
    <div class="row banner-content align-items-center">
      <div class="col-12 text-center content-left col-lg-6 text-lg-left container">
        {!! $data->content !!}
        <p class="text-center text-lg-left">
          <a class="btn btn-outline-primary" href="{{$data->link->url}}">{{$data->link->title}}</a>
        </p>
      </div>
      <div class="col-12 image-right col-lg-6">
        <div class="img-big lazy bg" data-src="{{$data->imageBig}}"></div>
        <img src="{{IMG_BASE64}}" data-src="{{$data->imageSmall}}" alt="Archangel Bio" class="img-small lazy">
      </div>
    </div>
  </div>
</section>
