<section class="module mod-cta-banner">
  <div class="container-2 text-center">
    <div class="container-3">
      <img src="{{IMG_BASE64}}" data-src="{{$data->image}}" alt="cta" class="lazy">
      <div class="my-4">
        {!! $data->content !!}
      </div>
      <div class="d-flex flex-column flex-md-row justify-content-center align-items-center">
        <a href="{{$data->link1}}" class="btn btn-primary">Get a free DNA analysis</a>
        <a href="{{$data->link1}}" class="btn btn-outline-primary mt-3 mt-md-0 ml-md-3">Contact us today</a>
      </div>
    </div>
  </div>
</section>
