<section class="module mod-industries">
  <div class="container-2">
    <div class="industries-title container p-xl-0">
      {!! $data->title !!}
    </div>
    <div class="container p-xl-0">
      <div class="industry-cards row">
        @foreach ($data->industries as $industry)
          <div class="industry-card col-12 touch col-md-6 col-xl-3">
            <div class="industry-item overlay-after">
              <div class="bg-industry lazy bg ps-as" data-src="{{$industry->image}}"></div>
              <div class="industry-content">
                <div class="ps-rv w-100 d-flex align-items-center justify-content-between px-3">
                  <div class="col-6">
                    <h3>{{$industry->title}}</h3>
                  </div>
                  <img class="lazy col-6 industry-shiled pr-0" src="{{IMG_BASE64}}" data-src="{{$industry->shield_image}}" alt="{{$industry->title}}">
                </div>
              </div>
            </div>
          </div>
        @endforeach
      </div>
    </div>
</section>